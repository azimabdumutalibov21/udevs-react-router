import "./App.css";
import { BrowserRouter as Router, Routes, Route } from "react-router-dom";
import Home from "./component/Home/Home";
import Blogs from "./component/Blogs/Blogs";

function App() {
  return (
    <Router>
      <div className="App">
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path={"/blogs"} element={<Blogs />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
