import axios from 'axios';
import React, { useEffect, useState } from 'react'

export default function ItemAxios() {
    const [posts, setPosts] = useState([]);

    useEffect(() =>{
        getPosts();
    }, []);

    function getPosts() {
        axios
        .get('https://jsonplaceholder.typicode.com/posts')
        .then(({ data }) => {
            setPosts(data);
        })
        .catch((error) => console.error(error))
    }
    return (
        <div>
            {posts.map((item) => (
                <li key={item.id}>{item.title}</li>
            )).splice(0,6)}
        </div>
    )
}
